.. _RASCIL_api:

API
===

Here is a quick guide to the layout of the package:

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com
