.. _documentation_master:

.. toctree::

eMERLIN RASCIL Pipeline (ERP)
#############################

The Radio Astronomy Simulation, Calibration and Imaging Library expresses radio interferometry calibration and
imaging algorithms in python and numpy. The interfaces all operate with familiar data
structures such as image, visibility table, gaintable, etc. The python source code is directly accessible from these
documentation pages: see the source link in the top right corner.

.. toctree::
   :maxdepth: 2

   ERP_install
   ERP_examples
   ERP_directory
   ERP_api

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com
