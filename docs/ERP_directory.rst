
.. toctree::
   :maxdepth: 2

Directory
*********

This directory is designed to to help those familiar with other calibration and imaging packages navigate the ERP. Not
all functions are listed here. The API contains all functions.

The long form of the name is given for all entries but all function names arre unique so a given function can be
accessed using the very top level import::

   import erp.data_models
   import erp.processing_components
   import erp.workflows
