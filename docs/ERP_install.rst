.. _ERP_install:

Installation
============

ERP can be run on a Linux or macos machine or cluster of machines. At least 16GB physical memory is necessary to run
the full test suite. In general more memory is better. RASCIL uses Dask for multi-processing and can make good use of multi-core and multi-node machines.

Installation via docker
+++++++++++++++++++++++

If you are familar with docker, an easy approach is to use our docker scripts:

.. toctree::
   :maxdepth: 2

   installation/ERP_docker

.. _feedback: mailto:realtimcornwell@gmail.com
