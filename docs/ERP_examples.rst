.. _erp_examples:

Examples
========

Running notebooks
*****************

The best way to get familiar with ERP is via jupyter notebooks. For example::

   jupyter examples/notebooks/eMERLIN_imaging.ipynb


See the jupyter note books below:

   examples/notebooks/eMERLIN_imaging.rst

.. toctree::
   :maxdepth: 1

   examples/notebooks/eMERLIN_imaging.rst

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com

