# eMERLIN
Temporary home for eMERLIN scripts

The first version of a notebook in is exploratory/script1/eMERLIN_imaging.ipynb. This run ICAL on the 
calibrated data.

This illustrates how RASCIL may be used to image eMERLIN data. The example data is from:

http://www.e-merlin.ac.uk/distribute/support/tutorials/3C277.1_20150505.tar

For a summary of the images produced by the eMERLIN CASA pipeline see:

http://www.e-merlin.ac.uk/distribute/support/tutorials/3C277.1_20150505/weblog/index.html

